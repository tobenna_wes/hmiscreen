package main

import (
	"fmt"
	"image/color"
	"net/url"

	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

func main() {
	a := app.New()
	w := a.NewWindow("Test HMI Screen")
	w.SetMaster() // Sets window that closes app when dismissed

	//Menu Items along with the respective function to call
	linkedInItem := fyne.NewMenuItem("View LinkedIn", func() {
		u, _ := url.Parse("https://www.linkedin.com/in/tobennawes/")
		_ = a.OpenURL(u)

	})

	websiteItem := fyne.NewMenuItem("View Website", func() {
		u, _ := url.Parse("https://www.tobennawes.com/")
		_ = a.OpenURL(u)

	})

	//Menu SubMenu that is added to the Main menu
	aboutMenu := fyne.NewMenu("About Me", linkedInItem, websiteItem)

	//initialization of the main Menu
	topMainMenu := fyne.NewMainMenu(aboutMenu)

	//Attaches the menu to the window
	w.SetMainMenu(topMainMenu)

	//The icon used to display the status of app using color
	statusIcon := &canvas.Circle{StrokeColor: color.NRGBA{0xff, 0xff, 0xff, 0xff},
		FillColor:   color.NRGBA{0xff, 0x00, 0x00, 0xff},
		StrokeWidth: 0}

	//Wrap of status Icon so we can determine its size
	statusIconWrap := fyne.NewContainerWithLayout(layout.NewGridWrapLayout(fyne.NewSize(20, 20)), statusIcon)

	//Info Labels
	welcomeLabel := widget.NewLabelWithStyle("Welcome to my Hmi demo", fyne.TextAlignCenter, fyne.TextStyle{Bold: true})
	statusLabel := widget.NewLabelWithStyle("Status:", fyne.TextAlignCenter, fyne.TextStyle{Bold: true})

	//Fun section to simulate connected status
	var powerOnButton *widget.Button
	isPoweredOn := false
	powerOnFunction := func() {

		if isPoweredOn {
			isPoweredOn = false
			statusLabel.SetText("Status: ")
			powerOnButton.SetText("Power On Decoder")
			statusIcon.FillColor = color.NRGBA{0xff, 0x00, 0x00, 0xff}
		} else {
			isPoweredOn = true
			statusLabel.SetText("Status: Powered On")
			powerOnButton.SetText("Power off Decoder")
			statusIcon.FillColor = color.NRGBA{0x00, 0xff, 0x00, 0xff}
		}
		statusIcon.Refresh()
	}
	//Power on Button
	powerOnButton = widget.NewButton("Power On Decoder", powerOnFunction)

	//Fun section to simulate Data Collection
	collectDataButton := widget.NewButton("Collect CAN Bus Data", func() {
		statusIcon.FillColor = color.NRGBA{0xff, 0x45, 0x00, 0xff}
		statusLabel.SetText("Status: Collecting and Decoding Bus Data")
		statusIcon.Refresh()
		time.Sleep(2 * time.Second)

		if isPoweredOn {
			statusIcon.FillColor = color.NRGBA{0x00, 0xff, 0x00, 0xff}
			statusLabel.SetText("Status: Successful")
		} else {
			statusIcon.FillColor = color.NRGBA{0xff, 0x00, 0x00, 0xff}
			statusLabel.SetText("Error: Tester not connected")
		}

		statusIcon.Refresh()
		fmt.Print("Collects CAN Bus Data")
	})

	//Grid that contains both Status and Indicator
	statusGrid := container.NewGridWithColumns(2, statusLabel, container.NewCenter(statusIconWrap))

	//Grid that contains buttons
	buttons := container.NewGridWithColumns(2,
		powerOnButton, collectDataButton)

	//The Container that is attached to the window and displayed
	InfoContainter := container.NewVBox(welcomeLabel, statusGrid, buttons)

	w.SetContent(InfoContainter)
	w.Resize(fyne.NewSize(800, 800))
	w.ShowAndRun()
}
